class A {

    constructor(z) {
        this.x = 3;
        this.y = 10;
        this.z = z;
    }

    static getPI() {
        return 3.14159;
    }

    suma() {
        return this.x + this.y + this.z;
    }

    set zeta(z) {
        this.z = z * 2;
    }

    get zeta() {
        return this.z / 2;
    }
}

let a = new A(20);

a.zeta = 15;

console.log(a.zeta);

console.log(A.getPI());

class B extends A {
    constructor() {
        super(100);
        this.x = 20;
    }

    suma() {
        return this.x + this.z;
    }

    resta() {
        return this.x - this.z;
    }
}

let b = new B();

console.log(b.suma());
console.log(b.resta());