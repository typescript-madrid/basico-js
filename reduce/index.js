let nums = [2, 4, 10, 15, 12];

let suma = nums.reduce((x, y) => x + y);

let objs = [
    {
        x: 3,
        y: 2
    },
    {
        x: 8,
        y: 10
    },
    {
        x: 10,
        y: 15
    }
]

let sumaX = objs.reduce((x, o2) => x + o2.x, 0);            // Método 1
let sumaX = objs.map(o => o.x).reduce((x, y) => x + y);     // Método 2